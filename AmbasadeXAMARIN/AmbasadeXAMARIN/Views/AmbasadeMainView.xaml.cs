﻿using AmbasadeXAMARIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmbasadeXAMARIN.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AmbasadeMainView : MasterDetailPage
    {
        public List<MasterPageItem> menu { get; set; }
        public AmbasadeMainView ()
		{
			InitializeComponent ();
            menu = new List<MasterPageItem>();
            var embassiesView = new MasterPageItem() { Titulo = "Embassies", PaginaHija = typeof(EmbassiesListView) };
            var embassyAddView = new MasterPageItem() { Titulo = "Add Embassy", PaginaHija = typeof(EmbassyAddView) };
            //var embassyEditView = new MasterPageItem() { Titulo = "Edit Embassy", PaginaHija = typeof(EmbassyEditView) };
            //var embassyDetailsView = new MasterPageItem() { Titulo = "Details Embassy", PaginaHija = typeof(EmbassyDetailsView) };

            menu.Add(embassiesView);
            menu.Add(embassyAddView);
            //menu.Add(embassyEditView);
            //menu.Add(embassyDetailsView);

            this.lsvMenu.ItemsSource = menu;
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(EmbassiesListView)));
            this.lsvMenu.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.PaginaHija;
            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;
        }
	}
}