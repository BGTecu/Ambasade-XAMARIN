﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AmbasadeXAMARIN.Models;

namespace AmbasadeXAMARIN.Helpers
{
    public class HelperAPI
    {
        //private const String ApiURL = "http://localhost:50693/api/Embassy/";
        private const String ApiURL = "http://ambasadeapi.azurewebsites.net/api";

        private HttpClient CrearCliente()
        {
            var clientehttp = new HttpClient();
            clientehttp.DefaultRequestHeaders.Accept.Clear();
            clientehttp.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return clientehttp;
        }

        #region EMBASSY API
        public async Task<List<Embassy>> GetEmbassies()
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Embassy", string.Empty));
            HttpResponseMessage response = await client.GetAsync(UriApi);
            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                List<Embassy> eList = JsonConvert.DeserializeObject<List<Embassy>>(contenido);
                return eList;
            }
            else { return null; }
        }

        public async Task<Embassy> GetEmbassyByID(int id)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Embassy/" + id, string.Empty));
            HttpResponseMessage response = await client.GetAsync(UriApi);

            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                Embassy embassy = JsonConvert.DeserializeObject<Embassy>(contenido);
                return embassy;
            }
            else { return null; }
        }

        public async Task<Embassy> GetEmbassyByCountryID(int id)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/EmbassyCountry/" + id, string.Empty));
            HttpResponseMessage response = await client.GetAsync(UriApi);

            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                Embassy embassy = JsonConvert.DeserializeObject<Embassy>(contenido);
                return embassy;
            }
            else { return null; }
        }

        public async Task AddEmbassy(Embassy e)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Embassy", string.Empty));
            var json = JsonConvert.SerializeObject(e);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            byte[] buffer = Encoding.UTF8.GetBytes(json);
            ByteArrayContent content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(UriApi, content);

            if (response.IsSuccessStatusCode)
            {
                //await DisplayAlert("Alert", "Embassy successfully saved!", "OK");
            }
        }

        public async Task EditEmbassy(Embassy e)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Embassy", string.Empty));
            var json = JsonConvert.SerializeObject(e);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            byte[] buffer = Encoding.UTF8.GetBytes(json);
            ByteArrayContent content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PutAsync(UriApi, content);

            if (response.IsSuccessStatusCode)
            {
                //await DisplayAlert("Alert", "Embassy successfully modified!", "OK");
            }
        }

        public async Task DeleteEmbassy(int id)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Embassy/" + id, string.Empty));
            HttpResponseMessage response = await client.DeleteAsync(UriApi);

            if (response.IsSuccessStatusCode)
            {
                //await DisplayAlert("Alert", "Embassy successfully deleted!", "OK");
            }
        }
        #endregion

        #region COUNTRIES & CONTINENTS API
        public async Task<List<Countries>> GetCountries()
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Countries", string.Empty));
            HttpResponseMessage response = await client.GetAsync(UriApi);

            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                List<Countries> countries = JsonConvert.DeserializeObject<List<Countries>>(contenido);
                return countries;
            }
            else { return null; }
        }

        public async Task<List<Continents>> GetContinents()
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/Continents", string.Empty));
            HttpResponseMessage response = await client.GetAsync(UriApi);

            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                List<Continents> continents = JsonConvert.DeserializeObject<List<Continents>>(contenido);
                return continents;
            }
            else { return null; }
        }
        #endregion

        #region SECURITY TOKEN API
        public async Task<UserToken> GetToken(String user, String pass)
        {
            HttpClient client = CrearCliente();
            var UriApi = new Uri(string.Format(ApiURL + "/SecurityToken", string.Empty));

            KeyValuePair<string, string> grant = new KeyValuePair<string, string>("grant_type", "password");
            KeyValuePair<string, string> username = new KeyValuePair<string, string>("username", user);
            KeyValuePair<string, string> password = new KeyValuePair<string, string>("password", pass);
            FormUrlEncodedContent form = new FormUrlEncodedContent(new[] { grant, username, password });

            HttpResponseMessage response = await client.PostAsync(UriApi, form);
            if (response.IsSuccessStatusCode)
            {
                var contenido = await response.Content.ReadAsStringAsync();
                UserToken token = JsonConvert.DeserializeObject<UserToken>(contenido);

                return token;
            }
            else { return null; }
        }
        #endregion
    }
}
