﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmbasadeXAMARIN.Models
{
    public class Countries
    {
        public int IdCountry { get; set; }

        public string CountryName { get; set; }

        public int IdContinent { get; set; }

        public string CountryImage { get; set; }

        public string CountryURLImage { get; set; }

    }
}
