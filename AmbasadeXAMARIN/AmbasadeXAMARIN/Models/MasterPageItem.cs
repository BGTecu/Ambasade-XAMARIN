﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmbasadeXAMARIN.Models
{
    public class MasterPageItem
    {
        public string Titulo { get; set; }
        public Type PaginaHija { get; set; }
    }
}
