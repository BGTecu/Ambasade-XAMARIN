﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmbasadeXAMARIN.Models
{
    public class Embassy
    {
        public int EmbassyID { get; set; }

        public string EmbassyName { get; set; }

        public string AmbassadorName { get; set; }

        public string WebSite { get; set; }

        public string Address { get; set; }

        public string AddressInfo { get; set; }

        public string GeoLocation { get; set; }

        public string MapURL { get; set; }

        public string StreetViewURL { get; set; }

        public string PhotoName { get; set; }

        public string PhotoURL { get; set; }

        public string Phone { get; set; }

        public string EmergencyPhone { get; set; }

        public string Fax { get; set; }

        public string eMail { get; set; }

        public string Duminică { get; set; }

        public string Luni { get; set; }

        public string Marţi { get; set; }

        public string Miercuri { get; set; }

        public string Joi { get; set; }

        public string Vineri { get; set; }

        public string Sambătă { get; set; }

        public string AuthorizedToEdit { get; set; }

        public string RegisterAddedBy { get; set; }

        public bool Published { get; set; }

        public int IdCountry { get; set; }
    }
}
