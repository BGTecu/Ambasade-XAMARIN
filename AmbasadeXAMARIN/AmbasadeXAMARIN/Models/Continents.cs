﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmbasadeXAMARIN.Models
{
    public class Continents
    {
        public int IdContinent { get; set; }

        public string Nombre { get; set; }
    }
}
