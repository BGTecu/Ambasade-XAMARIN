﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace AmbasadeXAMARIN.Models
{
    public class UserToken
    {
        [JsonProperty("access_token")]
        public String AccessToken { get; set; }
        [JsonProperty("token_type")]
        public String TokenType { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        public DateTime ExpiresTime { get; set; }

        public String UserName { get; set; }
    }
}
