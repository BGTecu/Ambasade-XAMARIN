﻿using AmbasadeXAMARIN.Helpers;
using AmbasadeXAMARIN.Models;
using AmbasadeXAMARIN.ViewModels.Base;
using AmbasadeXAMARIN.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmbasadeXAMARIN.ViewModels
{
    public class EmbassiesViewModel : ViewModelBase
    {
        public EmbassiesViewModel()
        {
            HelperAPI helper = new HelperAPI();
            Task.Run(async () => {
                List<Embassy> lista = await helper.GetEmbassies();
                this.Embassies = new ObservableCollection<Embassy>(lista);
            });
        }

        private ObservableCollection<Embassy> _Embassies;
        public ObservableCollection<Embassy> Embassies
        {
            get { return this._Embassies; }
            set
            {
                this._Embassies = value;
                OnPropertyChanged("Embassies");
            }
        }

        private Embassy _SelectedEmbassy;
        public Embassy SelectedEmbassy
        {
            get { return this._SelectedEmbassy; }
            set
            {
                this._SelectedEmbassy = value;
                OnPropertyChanged("SelectedEmbassy");
            }
        }

        public Command EmbassyDetails
        {
            get
            {
                return new Command(async () => {
                    EmbassyDetailsView view = new EmbassyDetailsView();
                    EmbassyDetailsViewModel viewmodel = new EmbassyDetailsViewModel();
                    viewmodel.Embassy = this.SelectedEmbassy;
                    view.BindingContext = viewmodel;
                    await Application.Current.MainPage.Navigation.PushModalAsync(view);
                });
            }
        }
    }
}
