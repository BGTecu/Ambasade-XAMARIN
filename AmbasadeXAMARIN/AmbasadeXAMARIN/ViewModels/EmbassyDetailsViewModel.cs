﻿using AmbasadeXAMARIN.Helpers;
using AmbasadeXAMARIN.Models;
using AmbasadeXAMARIN.ViewModels.Base;
using AmbasadeXAMARIN.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmbasadeXAMARIN.ViewModels
{
    public class EmbassyDetailsViewModel : ViewModelBase
    {
        HelperAPI helper;
        public EmbassyDetailsViewModel()
        {
            this.helper = new HelperAPI();
            _Embassy = new Embassy();
        }

        private Embassy _Embassy;
        public Embassy Embassy
        {
            get { return this._Embassy; }
            set
            {
                this._Embassy = value;
                OnPropertyChanged("Embassy");
            }
        }

        public Command DeleteEmbassy
        {
            get
            {
                return new Command(async () =>
                {
                    await helper.DeleteEmbassy(this.Embassy.EmbassyID);
                    await Application.Current.MainPage.Navigation.PopModalAsync();
                });
            }
        }

        public Command EditEmbassy
        {
            get
            {
                return new Command(async () =>
                {
                    EmbassyEditView view = new EmbassyEditView();
                    EmbassyDetailsViewModel viewmodel = new EmbassyDetailsViewModel();
                    viewmodel.Embassy = this.Embassy;
                    view.BindingContext = viewmodel;
                    await Application.Current.MainPage.Navigation.PushAsync(view);
                });
            }
        }

        public Command EditEmbassySave
        {
            get
            {
                return new Command(async () =>
                {
                    await helper.EditEmbassy(Embassy);
                    await Application.Current.MainPage.Navigation.PopModalAsync();
                });
            }
        }

        public Command AddEmbassy
        {
            get
            {
                return new Command(async () =>
                {
                    _Embassy.IdCountry = 802;
                    _Embassy.Published = true;
                    await helper.AddEmbassy(Embassy);
                    await Application.Current.MainPage.Navigation.PopModalAsync();
                });
            }
        }
    }
}
